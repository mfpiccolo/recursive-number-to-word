class Number

  def initialize(number)
    @number = number
    @set_divider = '1'
    if (@number.to_s.length % 3) == 0
      (@number.to_s.length - 3).times {|n| @set_divider << '0'}
    else
      (@number.to_s.length - (@number.to_s.length % 3)).times {|n| @set_divider << '0'}
    end
    @ones = %w(zero one two three four five six seven eight nine ten eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen)
    @tens = %w(junk junka twenty thirty forty fifty sixty seventy eighty ninety)
    @big_nums = %w(thousand million billion trillion quadrillion qintiollion sextilliion septillion octillion nonillion decillion)
  end

  def less_than_thousand
    if @number == 0
      @ones[@number]
    elsif @number < 20
      @ones[@number]
    elsif @number < 100
      ones = @ones[@number % 10]
      tens = @tens[@number/10]
      tens + '-' + ones
    elsif @number < 1000
      hundreds = @ones[@number/100]
      hundreds + " hundred " + Number.new(@number % 100).to_words
    end
  end

  def to_words
    # binding.pry
    if @number < 1000
      less_than_thousand
    else
  
      Number.new(@number/(@set_divider.to_i)).to_words + " " + @big_nums[(((@set_divider.length - 1)/3) - 1)] + " " + Number.new(@number % (@set_divider.to_i)).to_words
    end
  end

  #   elsif @number < 1000000
  #     thousands = Number.new(@number/1000).to_words
  #     thousands + " thousand " + Number.new(@number % 1000).to_words
  #   elsif @number < 1000000000
  #     millions = Number.new(@number/1000000).to_words
  #     millions + " million " + Number.new(@number % 1000000).to_words
  #   elsif @number < 1000000000000
  #     billions = Number.new(@number/1000000000).to_words
  #     billions + " billion " + Number.new(@number % 1000000000).to_words
  #   elsif @number < 1000000000000000
  #     trillions = Number.new(@number/1000000000000).to_words
  #     trillions + " trillion " + Number.new(@number % 1000000000000).to_words
  #   end
  # end

end
