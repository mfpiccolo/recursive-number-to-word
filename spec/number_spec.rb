require 'rspec'
require './lib/number'
require 'pry'
require 'pry-debugger'

describe Number do 

  # context '#initialize' do 
  #   it 'initilializes with a number' do 
  #     Number.new(15).should be_an_instance_of Number
  #   end
  # end

  context '#to_words' do 
    it 'converts a single digit number to words' do 
      Number.new(1).to_words.should eq 'one'
    end

    it 'converts 42 to forty two' do
      Number.new(42).to_words.should eq 'forty-two'
    end

    it 'converts 542 to one hundred forty-two' do
      Number.new(542).to_words.should eq 'five hundred forty-two'
    end

    it 'converts 1342 to be one thousand three hundred and forty-two' do
      Number.new(1342).to_words.should eq 'one thousand three hundred forty-two'
    end

    it 'converts 43426 to be thirteen thousand four hundred twenty-six' do
      Number.new(43426).to_words.should eq 'forty-three thousand four hundred twenty-six'
    end 

    it 'converts 1,342,634 to one million three hundred thousand six hundred thirty-four' do 
      Number.new(1342634).to_words.should eq 'one million three hundred forty-two thousand six hundred thirty-four'
    end   

    it 'converts 9,342,634,777 to one million three hundred thousand six hundred thirty-four' do 
      Number.new(9342634777).to_words.should eq 'nine billion three hundred forty-two million six hundred thirty-four thousand seven hundred seventy-seven'
    end   

    it 'converts a huge number' do 
      Number.new(934263477923234342433342343849774666).to_words.should eq 'nine hundred thirty-four decillion two hundred sixty-three nonillion four hundred seventy-seven octillion nine hundred twenty-three septillion two hundred thirty-four sextilliion three hundred forty-two qintiollion four hundred thirty-three quadrillion three hundred forty-two trillion three hundred forty-three billion eight hundred forty-nine million seven hundred seventy-four thousand six hundred sixty-six'
    end   
  end
end
